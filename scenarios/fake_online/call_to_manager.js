/**
 * Метод, кастующий объект в строку для post запроса
 */
Object.defineProperty(
    Object.prototype,
    "serialize",
    {
        enumerable: false,
        value: function (prefix) {
            var str = [];

            for (var key in this) {
                if (this.hasOwnProperty(key)) {
                    var k = prefix ? prefix + "[" + key + "]" : key;
                    var v = this[key];

                    str.push((typeof v == "object" && v !== null) ? v.serialize(k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
                }
            }

            return str.join("&");
        }
    }
);

/**
 * Объект сценария звонка манагеру
 *
 * @param data
 * @constructor
 */
function CallToManagerScenario(data) {
    var self = this;

    self.clinic_manager_phone = null;
    self.language = Language.RU_RUSSIAN_FEMALE;
    self.call_record_url = null;

    self.call_id = null; //ид звонка из дд
    self.cc_phone = null; //телефон кол центра - подставляется как номер исходящего
    self.callback_url = null; //урл куда слать фидбек
    self.base_auth_user = null; //логин базовой авторизации
    self.base_auth_password = null; //пароль базовой авторизации

    //инициализируем переменные пришедшие из дд
    for (var k in data) {
        if (self.hasOwnProperty(k)) {
            self[k] = data[k];
        }
    }

    /**
     * Старт сценария
     */
    self.start = function(){
        // начало выполнения сценария - звоним на номер 1
        var call = VoxEngine.callPSTN(self.clinic_manager_phone, self.cc_phone);

        call.addEventListener(CallEvents.RecordStarted, function (e) {
            self.call_record_url = e.url;
        });

        call.record();

        // обработчики событий
        call.addEventListener(CallEvents.Connected, function () {
            // звонок соединен успешно, проигрываем сообщение
            call.say("Добрый день! DocDoc не удалось соединиться с вашим Контакным центром для записи пациентов. Мы произвели 3 попытки в последнее 10 минут. Срочно исправьте ситуацию, в противном случае ваши врачи могу быть писсимизированны. Спасибо.", self.language);

            call.addEventListener(CallEvents.PlaybackFinished, function () {
                self.requestToDocDocAndTerminate({
                    event: 'manager.disconnected'
                });
            });
        });

        call.addEventListener(CallEvents.Failed, function (e) {
            self.requestToDocDocAndTerminate({
                code: e.code,
                reason: e.reason,
                event: 'manager.failed'
            });
        });

        call.addEventListener(CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({
                event: 'manager.disconnected'
            });
        });
    };

    /**
     * Послать пост запрос в докдок и умереть
     *
     * @param params
     */
    self.requestToDocDocAndTerminate = function (params) {
        var self = this;

        params.call_id = self.call_id;
        params.call_record_url = self.call_record_url;

        var url = 'https://';

        if (self.base_auth_user && self.base_auth_password) {
            url += self.base_auth_user + ':' + self.base_auth_password + '@';
        }

        url += self.callback_url;

        Net.httpRequest(
            url,
            function (e) {
                // информация по реузльтатам запроса - e1.code, e1.text, e1.data, e1.headers
                // убиваем сессию
                Logger.write('Request to ' + self.callback_url + ' with data: ' + JSON.stringify(params) + ' completed with code ' + e.code);
                VoxEngine.terminate();
            },
            {
                method: 'POST',
                postData: params.serialize()
            }
        );
    };
}

/**
 * Подписываюсь на событие старта сценария
 */
VoxEngine.addEventListener(AppEvents.Started, function () {
    var scenario = new CallToManagerScenario(JSON.parse(VoxEngine.customData()));
    scenario.start();
});
