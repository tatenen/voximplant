require(Modules.IVR); // enable IVR module

/**
 * Объект сценария
 *
 * @param data
 *
 * @constructor
 */
function CallToClientScenario(data) {
    var self = this;

    //приходит из http запроса
    this.call_id = null; //ид звонка из дд
    this.clinic_phone = null; //телефон клиники
    this.client_phone = null; //телефон клиента
    this.cc_phone = null; //телефон кол центра - подставляется как номер с которого идет звонок
    this.callback_url = null; //урл куда слать фидбек
    this.base_auth_user = null; //логин базовой авторизации
    this.base_auth_password = null; //пароль базовой авторизации
    this.next_call_time = null;
    this.client_name = null;
    this.doctor_name = null;
    this.diagnostics = {};
    this.request_kind = null;
    this.date_admission = null;
    this.dd_sound_url = null;

    this.language = Language.RU_RUSSIAN_FEMALE; //дефолтный язык чтения текста
    this.timeToClinicPressButton = 600000; //10 мин таймаут ivr "нажмите 1 для соединения с клиентом"

    this.support_email = 'tech-docdoc@docdoc.ru';

    this.record_urls = []; // урлы аудиозаписей
    this.saying_return_text_to_client = false;
    // состояние звонка клиенту
    this.client_connected = false;
    this.debug = false;

    this.reInitEventListener = function (object, event, handler) {
        object.removeEventListener(event);
        object.addEventListener(event, handler);
    };

    //инициализируем переменные пришедшие из дд
    for (var k in data) {
        if (this.hasOwnProperty(k)) {
            this[k] = data[k];
        }
    }

    /**
     * Старт сценария
     */
    this.start = function () {
        // начало выполнения сценария - звоним на номер 1
        var callToClient = VoxEngine.callPSTN(self.client_phone, self.cc_phone);

        callToClient.addEventListener(CallEvents.Failed, function (e) {
            self.requestToDocDocAndTerminate({
                event: 'client.failed',
                code: e.code,
                reason: e.reason
            });
        });

        callToClient.addEventListener(CallEvents.RecordStarted, function (e) {
            self.record_urls.push(e.url);
        });

        // взял трубку
        callToClient.addEventListener(CallEvents.Connected, function () {
            self.client_connected = true;
            // начинаем записывать
            callToClient.record();

            callToClient.say('Вас приветствует сервис по поиску врачей DocDoc. В настоящий момент происходит обработка вашей заявки на запись. В течение двадцати секунд мы соединим вас с клиникой. Не вешайте пожалуйста трубку.', self.language);
            callToClient.addEventListener(CallEvents.PlaybackFinished, function () {
                callToClient.removeEventListener(CallEvents.PlaybackFinished);

                if (self.dd_sound_url) {
                    var url = 'https://';

                    if (self.base_auth_user && self.base_auth_password) {
                        url += self.base_auth_user + ':' + self.base_auth_password + '@';
                    }

                    url += self.dd_sound_url;

                    callToClient.startPlayback(url, true);
                } else {
                    callToClient.playProgressTone("RU");
                }
            });

            self.connectWithClinic(callToClient);
        });
    };

    /**
     * Начинает соединение с клиентом
     *
     * @param callToClient
     */
    this.connectWithClinic = function (callToClient) {
        // звоню в клинику
        var callToClinic = VoxEngine.callPSTN(self.clinic_phone, self.cc_phone);

        // фиксируем время начала звонка в клинику
        var timeCallToClinicStarted = new Date().getTime();

        /**
         * Устанавливаем таймер через который нужно вернуться к лиенту
         * Пытаемся дозвониться в клинику 30 секунд
         *
         * @type {number}
         */
        var forceReturnToClient = setTimeout(
            function () {
                self.returnToClient(callToClient);
            },
            40000// 40 sec
        );

        callToClient.addEventListener(CallEvents.Disconnected, function () {
            clearTimeout(forceReturnToClient);
            self.client_connected = false;
        });

        // не дозвонились в клинику
        callToClinic.addEventListener(CallEvents.Failed, function (e) {
            clearTimeout(forceReturnToClient);

            self.returnToClient(callToClient, {
                event: 'clinic.failed',
                code: e.code,
                reason: e.reason
            });
        });

        // клиника взяла трубку
        callToClinic.addEventListener(CallEvents.Connected, function () {
            // клиника слилась
            callToClinic.addEventListener(CallEvents.Disconnected, function () {
                self.returnToClient(callToClient, {
                    event: 'clinic.disconnected',
                    code: 1002,
                    reason: 'Клиника положила трубку до соединения с клиентом'
                });
            });

            callToClinic.addEventListener(CallEvents.RecordStarted, function (e) {
                self.record_urls.push(e.url);
            });

            callToClinic.record();

            self.sayHelloToClinic(callToClinic, callToClient, forceReturnToClient, timeCallToClinicStarted);
        });
    };

    /**
     * Ивр с приветсвием в клинику
     *
     * @param callToClinic
     * @param callToClient
     * @param forceReturnToClient таймер через который нужно вернуться к клиенту принудительно
     * @param timeCallToClinicStarted время старта звонка в клиникику
     */
    this.sayHelloToClinic = function (callToClinic, callToClient, forceReturnToClient, timeCallToClinicStarted) {
        var text = 'Портал ДокДок.' + self.buildRequestInfo();
        var timeout = 60000 * 10; // 10 min

        var ivr = new IVRState(
            "clinicConnectedIvr",
            {
                type: "inputfixed",
                inputLength: 1,
                prompt: {
                    say: text,
                    lang: self.language
                },
                timeout: 1000 //1 sec
            },
            function (number) {
                if (number == 1) {
                    clearTimeout(forceReturnToClient);
                    // возможно соединение произошло, когда клиенту еще не весь текст сказали. значит останется обработчик который надо удалить
                    callToClient.removeEventListener(CallEvents.PlaybackFinished);

                    if (self.client_connected) {
                        self.connectTwoCalls(callToClient, callToClinic);
                    } else {
                        self.reconnectClientIfDisconnected(callToClinic);
                    }
                } else {
                    ivr.enter(callToClinic);
                }
            },
            function () {
                // Timeout
                if ((timeCallToClinicStarted + timeout) < (new Date()).getTime()) {
                    self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
                        self.returnToClient(callToClient, {event: 'clinic.hello.ivr.timeout'});
                    });

                    callToClinic.hangup();
                } else {
                    ivr.enter(callToClinic);
                }
            }
        );

        ivr.enter(callToClinic);
    };

    /**
     * Кейс когда клиент отвалился в процессе ожидания дозвона в клинику
     *
     * @param callToClinic
     */
    this.reconnectClientIfDisconnected = function (callToClinic) {
        self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({event: 'clinic.disconnected.after.client.disconnected'});
        });

        callToClinic.say('Идет соединение с пациентом.', self.language);
        callToClinic.addEventListener(CallEvents.PlaybackFinished, function () {
            callToClinic.removeEventListener(CallEvents.PlaybackFinished);
            self.callToClientFromClinic(callToClinic);
        });
    };

    /**
     * Соединение двух звонков
     *
     * @param callToClient
     * @param callToClinic
     */
    this.connectTwoCalls = function (callToClient, callToClinic) {
        // соединяем два звонка - звук
        VoxEngine.sendMediaBetween(callToClient, callToClinic);
        // и сигнализацию
        VoxEngine.easyProcess(callToClient, callToClinic);

        self.reInitEventListener(callToClient, CallEvents.Disconnected, function () {
            self.getFeedbackFromClinic(callToClinic);
        });

        self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({event: 'call.finished.clinic.disconnected'});
        });
    };

    /**
     * Получить фидбек с клиники
     *
     * @param callToClinic
     */
    self.getFeedbackFromClinic = function (callToClinic) {
        var repeatCount = 1;

        self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({event: 'clinic.feedback.disconnected'});
        });

        var ivr = new IVRState(
            "ivrClinicFeedback",
            {
                type: "inputfixed",
                inputLength: 1,
                prompt: {
                    say: "Если удалось записать пациента на приём, нажмите 1. Если не удалось, нажмите 2. Чтобы перезвонить пациенту, нажмите 3.",
                    lang: self.language
                },
                timeout: 1000 // 1 sec
            },
            function (number) {
                if (number == 1 || number == 2) {
                    self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
                        self.requestToDocDocAndTerminate({event: 'clinic.feedback', number: number});
                    });

                    callToClinic.say('Статус заявки сохранён. До свидания.', self.language);

                    callToClinic.addEventListener(CallEvents.PlaybackFinished, function () {
                        callToClinic.hangup();
                    });
                } else if (number == 3) {
                    self.callToClientFromClinic(callToClinic);
                } else {
                    ivr.enter(callToClinic);
                }
            },
            function () {
                // Timeout
                if (repeatCount < 10) {
                    ivr.enter(callToClinic);
                    repeatCount++;
                } else {
                    self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
                        self.requestToDocDocAndTerminate({event: 'clinic.feedback.timeout'});
                    });

                    callToClinic.hangup();
                }
            }
        );

        ivr.enter(callToClinic);
    };

    /**
     * Клиника звонит клиенту
     *
     * @param callToClinic
     */
    this.callToClientFromClinic = function (callToClinic) {
        var callToClient = VoxEngine.callPSTN(self.client_phone, self.cc_phone);
        callToClinic.playProgressTone("RU");

        self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({
                event: 'clinic.disconnected.when.connecting.to.client'
            });
        });

        self.reInitEventListener(callToClient, CallEvents.Failed, function () {
            self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
                self.requestToDocDocAndTerminate({event: 'client.failed.when.clinic.connecting'});
            });

            callToClinic.say('Не удалось соединиться с пациентом. Мы перезвоним Вам позже', self.language);
            self.reInitEventListener(callToClinic, CallEvents.PlaybackFinished, function () {
                callToClinic.hangup();
            });
        });

        callToClient.addEventListener(CallEvents.Connected, function () {
            self.connectTwoCalls(callToClient, callToClinic);
        });
    };

    /**
     * Вернуться к клиенту после таймаута ожидания клиники
     *
     * @param callToClient
     * @param params
     */
    this.returnToClient = function (callToClient, params) {
        if (self.client_connected) {
            self.reInitEventListener(callToClient, CallEvents.Disconnected, function () {
                self.client_connected = false;
                self.saying_return_text_to_client = false;

                if (params === undefined) {
                    // pass
                } else {
                    self.requestToDocDocAndTerminate(params);
                }
            });

            var text = 'К сожалению нам не удалось связаться с клиникой. Мы перезвоним Вам через ';

            if (self.next_call_time) {
                text += self.next_call_time + '.';
            } else {
                text += '20 минут.';
            }

            if (!self.saying_return_text_to_client) {
                callToClient.say(text, self.language);
                self.saying_return_text_to_client = true;

                callToClient.addEventListener(CallEvents.PlaybackFinished, function () {
                    callToClient.hangup();
                });
            }
        } else {
            self.requestToDocDocAndTerminate(params);
        }
    };

    /**
     * Послать пост запрос в докдок и умереть
     *
     * @param params
     */
    this.requestToDocDocAndTerminate = function (params) {
        params.call_id = self.call_id;
        params.record_urls = self.record_urls;

        var url = 'https://';

        if (self.base_auth_user && self.base_auth_password) {
            url += self.base_auth_user + ':' + self.base_auth_password + '@';
        }

        url += self.callback_url;

        var params_as_post_string = params.serialize();

        Net.httpRequest(
            url,
            function (e) {
                var params_as_json = JSON.stringify(params);

                // информация по реузльтатам запроса - e1.code, e1.text, e1.data, e1.headers
                // убиваем сессию
                Logger.write('Request to ' + self.callback_url + ' with data: ' + params_as_json + ' completed with code ' + e.code);

                if (self.debug) {
                    var debug_mailer = new Mailer();
                    debug_mailer.sendMail(
                        'a.tyutyunnikov@gmail.com',
                        'Debug',
                        'Запрос к ' + self.callback_url + ' завершился кодом ' + e.code + "\n" + "Тело запроса \n\n" + "json: " + params_as_json + "\n\n" + "post: " + params_as_post_string,
                        function () {
                            VoxEngine.terminate();
                        }
                    );
                }

                if (e.code != 200) {
                    var mailer = new Mailer();
                    mailer.sendMail(
                        self.support_email,
                        'Запрос к ' + self.callback_url + ' завершен с ошибкой',
                        'Запрос к ' + self.callback_url + ' завершился кодом ' + e.code + "\n" +
                        "Тело запроса \n\n" +
                        "json: " + params_as_json + "\n\n" +
                        "post: " + params_as_post_string,
                        function () {
                            VoxEngine.terminate();
                        }
                    );
                } else {
                    VoxEngine.terminate();
                }
            },
            {
                method: 'POST',
                postData: params_as_post_string
            }
        );
    };

    /**
     * Генерет информацию о записи
     *
     * @returns {string}
     */
    this.buildRequestInfo = function () {
        var client_info = 'На линии пациент: ';

        if (this.client_name) {
            client_info += this.client_name + '.';
        }

        client_info += 'Запись';

        if (this.date_admission) {
            client_info += ' на ' + this.date_admission;
        }

        if (this.request_kind == 'doctor') {
            if (this.doctor_name) {
                client_info += ' к врачу ' + this.doctor_name;
            }
        } else {
            if (this.diagnostics.up) {
                client_info += ' на диагностику ' + this.diagnostics.up;
            }

            if (this.diagnostics.down) {
                client_info += ' вид услуги ' + this.diagnostics.down + '.';
            }
        }

        return client_info;
    };
}

function Mailer() {
    var self = this;

    self.url = 'https://api.mailgun.net/v3/docdoc.ru/messages';
    self.login = 'api';
    self.password = 'key-3737a2f80442cf5c6cdfad5622430a92';

    this.sendMail = function (to, subject, body, callback) {
        Net.httpRequest(
            self.url,
            function (e) {
                Logger.write('mail with subject ' + subject + ' sended. code= ' + e.code);

                if (typeof callback === "function") {
                    callback();
                }
            },
            {
                method: 'POST',
                postData: {
                    'from': 'noreply@docdoc.ru',
                    'to': to,
                    'subject': '[Voximplant] ' + subject,
                    'text': body,
                    'o:tracking': false
                }.serialize(),
                'headers': [
                    "Authorization: Basic " + base64_encode(this.login + ":" + this.password)
                ]
            }
        );
    };
}

/**
 * Метод, кастующий объект в строку для post запроса
 */
Object.defineProperty(
    Object.prototype,
    "serialize",
    {
        enumerable: false,
        value: function (prefix) {
            var str = [];

            for (var key in this) {
                if (this.hasOwnProperty(key)) {
                    var k = prefix ? prefix + "[" + key + "]" : key;
                    var v = this[key];

                    str.push((typeof v == "object" && v !== null) ? v.serialize(k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
                }
            }

            return str.join("&");
        }
    }
);

/**
 * Подписываюсь на событие старта сценария
 */
VoxEngine.addEventListener(AppEvents.Started, function () {
    // в сценарий можно передать данные с помощью механизма customData,
    var scenario = new CallToClientScenario(JSON.parse(VoxEngine.customData()));
    scenario.start();
});

