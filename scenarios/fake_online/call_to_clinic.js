require(Modules.IVR); // enable IVR module

/**
 * Метод, кастующий объект в строку для post запроса
 */
Object.defineProperty(
    Object.prototype,
    "serialize",
    {
        enumerable: false,
        value: function (prefix) {
            var str = [];

            for (var key in this) {
                if (this.hasOwnProperty(key)) {
                    var k = prefix ? prefix + "[" + key + "]" : key;
                    var v = this[key];

                    str.push((typeof v == "object" && v !== null) ? v.serialize(k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
                }
            }

            return str.join("&");
        }
    }
);

/**
 * Объект сценария
 *
 * @param data
 *
 * @constructor
 */
function CallToClinicScenario(data) {
    var self = this;

    //приходит из http запроса
    this.call_id = null; //ид звонка из дд
    this.clinic_phone = null; //телефон клиники
    this.client_phone = null; //телефон клиента
    this.cc_phone = null; //телефон кол центра - подставляется как номер с которого идет звонок
    this.callback_url = null; //урл куда слать фидбек
    this.base_auth_user = null; //логин базовой авторизации
    this.base_auth_password = null; //пароль базовой авторизации
    this.client_next_call_time = null;
    this.client_name = null;
    this.doctor_name = null;
    this.diagnostics = {};
    this.request_kind = null;
    this.date_admission = null;

    this.language = Language.RU_RUSSIAN_FEMALE; //дефолтный язык чтения текста
    this.timeToClinicPressButton = 600000; //10 мин таймаут ivr "нажмите 1 для соединения с клиентом"
    this.call_record_url = null; //урл аудио записи

    this.support_email = 'tech-docdoc@docdoc.ru';

    this.reInitEventListener = function (object, event, handler) {
        object.removeEventListener(event);
        object.addEventListener(event, handler);
    };

    //инициализируем переменные пришедшие из дд
    for (var k in data) {
        if (this.hasOwnProperty(k)) {
            this[k] = data[k];
        }
    }

    /**
     * Старт сценария
     */
    this.start = function () {
        // начало выполнения сценария - звоним на номер 1
        var callToClinic = VoxEngine.callPSTN(self.clinic_phone, self.cc_phone);

        callToClinic.addEventListener(CallEvents.RecordStarted, function (e) {
            self.call_record_url = e.url;
        });

        // обработчики событий
        callToClinic.addEventListener(CallEvents.Connected, function () {
            callToClinic.addEventListener(CallEvents.Disconnected, function () {
                self.requestToDocDocAndTerminate({event: 'clinic.disconnected'});
            });

            callToClinic.record();

            // первый звонок соединен успешно, проигрываем сообщение
            //включаем ivr
            self.initIvrClinicMenu(callToClinic);
        });

        callToClinic.addEventListener(CallEvents.Failed, function (e) {
            self.requestToDocDocAndTerminate({
                event: 'clinic.failed',
                code: e.code,
                reason: e.reason
            });
        });
    };

    /**
     * Инициализация ivr "нажмите 1 для соединиеия с клиентом"
     *
     * @returns {IVRState}
     */
    this.initIvrClinicMenu = function (callToClinic) {
        //фиксируем дату включения ivr
        var timeDocDocIvrStarted = new Date().getTime();
        var request_info = self.buildRequestInfo();

        var clinicHelloMenuTimeout = function (ivr_state) {
            if ((timeDocDocIvrStarted + self.timeToClinicPressButton) < (new Date()).getTime()) {
                self.requestToDocDocAndTerminate({
                    event: 'clinic.menu.timeout'
                });
            } else {
                ivr_state.enter(callToClinic);
            }
        };

        /**
         * Последний ивр, который ждет нажатия 1 для соединения с клиентом
         *
         * @type {IVRState}
         */
        var press_button_ivr = new IVRState(
            "press_button_ivr",
            {
                type: "inputfixed",
                inputLength: 1,
                prompt: {
                    say: 'Для соединения с пациентом нажмите 1. Чтобы остановить звонки по текущей заявке - нажмите 3. Чтобы повторно прослушать информацию о пациенте нажмите решётку.',
                    lang: self.language
                },
                timeout: 1000 //1 sec
            },
            function (number) {
                if (number == 1) {
                    self.connectWithClient(callToClinic);
                } else if (number == '#') {
                    request_ivr.enter(callToClinic);
                } else if (number == 3) {
                    self.getRobotTurnOffReason(callToClinic);
                } else {
                    press_button_ivr.enter(callToClinic);
                }
            },
            function () {
                clinicHelloMenuTimeout(press_button_ivr);
            }
        );

        /**
         * Говорит информацию о заявке
         *
         * @type {IVRState}
         */
        var request_ivr = new IVRState(
            "request_ivr",
            {
                type: "inputfixed",
                inputLength: 1,
                prompt: {
                    say: request_info,
                    lang: self.language
                },
                nextState: press_button_ivr,
                timeout: 1000 //1 sec
            },
            function (number) {
                if (number == 1) {
                    self.connectWithClient(callToClinic);
                } else if (number == '#') {
                    request_ivr.enter(callToClinic);
                } else {
                    press_button_ivr.enter(callToClinic);
                }
            },
            function () {
                clinicHelloMenuTimeout(press_button_ivr);
            }
        );

        /**
         * Приветствие
         *
         * @type {IVRState}
         */
        var hello_ivr = new IVRState(
            "hello_ivr",
            {
                type: "inputfixed",
                inputLength: 1,
                prompt: {
                    say: 'Добрый день. Вас приветствует автоматическая система обработки заявок компании DocDoc. Чтобы прослушать информацию о пациенте нажмите 1. Для повтора сообщения нажмите решётку. Если у вас есть проблема с вводом, переключите ваш телефон в тоновый режим нажав звёздочку.',
                    lang: self.language
                },
                timeout: 1000 //1 sec
            },
            function () {
                request_ivr.enter(callToClinic);

            },
            function () {
                clinicHelloMenuTimeout(hello_ivr);
            }
        );

        hello_ivr.enter(callToClinic);
    };

    self.getRobotTurnOffReason = function(callToClinic) {
        self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({event: 'robot.turn_off'});
        });

        callToClinic.say('Назовите причину остановки', self.language);
        callToClinic.addEventListener(CallEvents.PlaybackFinished, function () {
            callToClinic.removeEventListener(CallEvents.PlaybackFinished);

            setTimeout(function () {
                callToClinic.say('До свидания.');
                callToClinic.addEventListener(CallEvents.PlaybackFinished, function () {
                    callToClinic.removeEventListener(CallEvents.PlaybackFinished);
                    callToClinic.hangup();
                });
            }, 5000);
        });
    };

    /**
     * Начинает соединение с клиентом
     */
    this.connectWithClient = function (callToClinic) {
        /**
         * Послать в докдок событие "не удалось дозвониться клиенту"
         *
         * @param code
         * @param reason
         */
        var sendClientFailedEventToDocDoc = function (code, reason) {
            self.requestToDocDocAndTerminate({
                event: 'client.failed',
                code: code,
                reason: reason
            });
        };

        var clientFailedHandler = function (code, reason) {
            //удаляю дисконект запрос
            self.reInitEventListener(
                callToClinic,
                CallEvents.Disconnected,
                function () {
                    sendClientFailedEventToDocDoc(code, reason);
                }
            );

            var clientFailedText = 'Не удалось соединиться с пациентом.';

            if (self.client_next_call_time) {
                clientFailedText += 'Мы перезвоним вам через ' + self.client_next_call_time + '. До свидания.';
            } else {
                clientFailedText += 'При желании, вы можете связаться с ним самостоятельно. Заявка будет считаться как не успешная. До свидания. ';
            }

            callToClinic.say(clientFailedText, self.language);
            callToClinic.addEventListener(CallEvents.PlaybackFinished, function () {
                    callToClinic.hangup();
            });
        };

        // звоню клиенту
        var callToClient = VoxEngine.callPSTN(self.client_phone, self.cc_phone);

        self.reInitEventListener(
            callToClinic,
            CallEvents.Disconnected,
            function () {
                sendClientFailedEventToDocDoc(1000, 'Клиника положила трубку в момент дозвона клиенту');
            }
        );

        /**
         * Устанавливаем таймер
         * Пытаемся дозвониться клиенту 30 секунд
         *
         * @type {number}
         */
        var clientCallTimer = setTimeout(
            function () {
                self.reInitEventListener(callToClient, CallEvents.Failed, function () {
                    clientFailedHandler(1001, 'Клиент не взял трубку в течение 30 секунд');
                });

                callToClient.hangup(); //перестаю звонить клиенту
            },
            30000 // 30 sec
        );

        //клиент взял трубку
        callToClient.addEventListener(CallEvents.Connected, function () {
            clearTimeout(clientCallTimer);
            self.connectTwoCalls(callToClient, callToClinic);
        });

        //не дозвонились клиенту
        self.reInitEventListener(callToClient, CallEvents.Failed, function (e2) {
            clearTimeout(clientCallTimer);

            clientFailedHandler(e2.code, e2.reason);
        });

        callToClinic.say('Оставайтесь на линии. Идет соединение с пациентом. После разговора не вешайте трубку.', self.language);
        callToClinic.addEventListener(CallEvents.PlaybackFinished, function () {
            callToClinic.removeEventListener(CallEvents.PlaybackFinished);

            //посылаем принудительные гудки
            callToClinic.playProgressTone("RU");
        });
    };

    /**
     * Инициализация ivr собирающего фидбек с клиники
     *
     * @returns {IVRState}
     */
    this.getFeedbackFromClinic = function (callToClinic) {
        var repeatCount = 1;

        //переопределяю поведение, если клиника положила трубку первой
        self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({event: 'clinic.feedback.disconnected'});
        });

        var ivr = new IVRState(
            "ivrClinicFeedback",
            {
                type: "inputfixed",
                inputLength: 1,
                prompt: {
                    say: "Если удалось записать пациента на приём, нажмите 1. Если не удалось, нажмите 2.  Чтобы перезвонить пациенту, нажмите 3.",
                    lang: self.language
                },
                timeout: 1000 //1 sec
            },
            function (number) {
                if (number == 1 || number == 2) {
                    self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
                        self.requestToDocDocAndTerminate({event: 'clinic.feedback', number: number});
                    });

                    callToClinic.say('Статус заявки сохранён. До свидания.', self.language);

                    callToClinic.addEventListener(CallEvents.PlaybackFinished, function () {
                        callToClinic.hangup();
                    });
                } else if (number == 3) {
                    self.connectWithClient(callToClinic);
                } else {
                    ivr.enter(callToClinic);
                }
            },
            function () {
                // Timeout
                if (repeatCount < 10) {
                    ivr.enter(callToClinic);
                    repeatCount++;
                } else {
                    self.requestToDocDocAndTerminate({event: 'clinic.feedback.timeout'});
                }
            }
        );

        ivr.enter(callToClinic);
    };

    /**
     * Соединение двух звонков
     *
     * @param callToClient
     * @param callToClinic
     */
    self.connectTwoCalls = function (callToClient, callToClinic) {
        // соединяем два звонка - звук
        VoxEngine.sendMediaBetween(callToClient, callToClinic);
        // и сигнализацию
        VoxEngine.easyProcess(callToClient, callToClinic);

        self.reInitEventListener(callToClient, CallEvents.Disconnected, function () {
            self.getFeedbackFromClinic(callToClinic);
        });

        self.reInitEventListener(callToClinic, CallEvents.Disconnected, function () {
            self.requestToDocDocAndTerminate({event: 'clinic.disconnected.after.forwarding'});
        });
    };

    /**
     * Послать пост запрос в докдок и умереть
     *
     * @param params
     */
    this.requestToDocDocAndTerminate = function (params) {
        params.call_id = self.call_id;
        params.call_record_url = self.call_record_url;

        var url = 'https://';

        if (self.base_auth_user && self.base_auth_password) {
            url += self.base_auth_user + ':' + self.base_auth_password + '@';
        }

        url += self.callback_url;

        var params_as_post_string = params.serialize();

        Net.httpRequest(
            url,
            function (e) {
                var params_as_json = JSON.stringify(params);

                // информация по реузльтатам запроса - e1.code, e1.text, e1.data, e1.headers
                // убиваем сессию
                Logger.write('Request to ' + self.callback_url + ' with data: ' + params_as_json + ' completed with code ' + e.code);

                if (e.code != 200) {
                    var mailer = new Mailer();
                    mailer.sendMail(
                        self.support_email,
                        'Запрос к ' + self.callback_url + ' завершен с ошибкой',
                        'Запрос к ' + self.callback_url + ' завершился кодом ' + e.code + "\n" +
                        "Тело запроса \n\n" +
                        "json: " + params_as_json + "\n\n" +
                        "post: " + params_as_post_string,
                        function () {
                            VoxEngine.terminate();
                        }
                    );
                } else {
                    VoxEngine.terminate();
                }
            },
            {
                method: 'POST',
                postData: params_as_post_string
            }
        );
    };

    /**
     * Генерет информацию о записи
     *
     * @returns {string}
     */
    this.buildRequestInfo = function () {
        var client_info = '';

        if (this.client_name) {
            client_info += 'Имя пациента ' + this.client_name + '.';
        }

        if (this.request_kind == 'doctor') {
            if (this.doctor_name) {
                client_info += 'Врач ' + this.doctor_name + '.';
            }
        } else {
            if (this.diagnostics.up) {
                client_info += 'Тип диагностики ' + this.diagnostics.up + '.';
            }

            if (this.diagnostics.down) {
                client_info += 'Вид услуги ' + this.diagnostics.down + '.';
            }
        }

        if (this.date_admission) {
            client_info += 'Запись на ' + this.date_admission + '.';
        }

        return client_info;
    };
}

function Mailer() {
    var self = this;

    self.url = 'https://api.mailgun.net/v3/docdoc.ru/messages';
    self.login = 'api';
    self.password = 'key-3737a2f80442cf5c6cdfad5622430a92';

    this.sendMail = function (to, subject, body, callback) {
        Net.httpRequest(
            self.url,
            function (e) {
                Logger.write('mail with subject ' + subject + ' sended. code= ' + e.code);

                if (typeof callback === "function") {
                    callback();
                }
            },
            {
                method: 'POST',
                postData: {
                    'from': 'noreply@docdoc.ru',
                    'to': to,
                    'subject': '[Voximplant] ' + subject,
                    'text': body,
                    'o:tracking': false
                }.serialize(),
                'headers': [
                    "Authorization: Basic " + base64_encode(this.login + ":" + this.password)
                ]
            }
        );
    };
}

/**
 * Подписываюсь на событие старта сценария
 */
VoxEngine.addEventListener(AppEvents.Started, function () {
    // в сценарий можно передать данные с помощью механизма customData,
    var scenario = new CallToClinicScenario(JSON.parse(VoxEngine.customData()));
    scenario.start();
});
